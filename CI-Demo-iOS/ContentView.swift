//
//  ContentView.swift
//  CI-Demo-iOS
//
//  Created by Антон on 17.02.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
        Text("Hello, world!")
            .padding()
            Text("How are you?")
                .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
