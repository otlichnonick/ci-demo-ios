//
//  ExampleView.swift
//  CI-Demo-iOS
//
//  Created by Антон on 17.02.2021.
//

import SwiftUI

struct ExampleView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ExampleView_Previews: PreviewProvider {
    static var previews: some View {
        ExampleView()
    }
}
