//
//  NewView.swift
//  CI-Demo-iOS
//
//  Created by Антон on 18.02.2021.
//

import SwiftUI

struct NewView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct NewView_Previews: PreviewProvider {
    static var previews: some View {
        NewView()
    }
}
